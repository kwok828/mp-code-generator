package com.kwok.mpgenerator.lang;

import org.springframework.http.HttpStatus;

/**
 * 业务枚举类
 */
public enum BusinessCode {
    /** 操作成功 */
    SUCCESS(HttpStatus.OK.value(), "请求成功"),
    ACCESS_FORBIDDEN(HttpStatus.FORBIDDEN.value(), "没有权限，请联系管理员授权"),
    DATA_CONFLICT(HttpStatus.CONFLICT.value(), "数据重复，请检查后提交"),
    /** 服务器内部错误 */
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "服务器内部错误"),
    /** 用户 */
    USER_OLDPWD_ERROR(50100, "旧密码不正确"),


    ;

    /**
     * 状态码
     */
    private int code;
    /**
     * 相关信息描述
     */
    private String msg;

    /**
     * 维护错误码+错误描述
     * @param code
     * @param msg
     */
    BusinessCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 获取错误码
     *
     * @return
     */
    public int getCode() {
        return code;
    }

    /**
     * 获取描述信息
     *
     * @return
     */
    public String getMessage() {
        return msg;
    }
}
