package com.kwok.mpgenerator.lang;

import lombok.Data;

import java.io.Serializable;

/**
 * 统一响应实体类
 */
@Data
public class ResponseDto<T> implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * 操作状态
     */
    private int code;

    /**
     * 状态附加消息
     */
    private String msg;

    /**
     * 响应内容
     */
    private T data;

    public ResponseDto() {
    }

    public ResponseDto(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> ResponseDto<T> response(int code, String msg, T data) {
        ResponseDto<T> responseDTO = new ResponseDto<>();
        responseDTO.setCode(code);
        responseDTO.setMsg(msg);
        responseDTO.setData(data);
        return responseDTO;
    }

    public static <T> ResponseDto<T> success() {
        return success(null);
    }

    public static <T> ResponseDto<T> success(T data) {
        return response(BusinessCode.SUCCESS, data);
    }

    public static <T> ResponseDto<T> response(BusinessCode businessCode) {
        return response(businessCode, null);
    }

    public static <T> ResponseDto<T> response(BusinessCode businessCode, T content) {
        return response(businessCode.getCode(), businessCode.getMessage(), content);
    }

    public static <T> ResponseDto<T> response(int code, String msg) {
        return response(code, msg, null);
    }

    public static <T> ResponseDto<T> error() {
        return error(BusinessCode.INTERNAL_SERVER_ERROR);
    }

    public static <T> ResponseDto<T> error(String msg) {
        return error(BusinessCode.INTERNAL_SERVER_ERROR.getCode(), msg, null);
    }

    public static <T> ResponseDto<T> error(BusinessCode businessCode) {
        return error(businessCode.getCode(), businessCode.getMessage(), null);
    }

    public static <T> ResponseDto<T> error(BusinessCode businessCode, T data) {
        return error(businessCode.getCode(), businessCode.getMessage(), data);
    }

    public static <T> ResponseDto<T> error(int code, String msg) {
        return error(code, msg, null);
    }

    public static <T> ResponseDto<T> error(int code, String msg, T data) {
        return new ResponseDto<T>(code, msg, data);
    }

}
