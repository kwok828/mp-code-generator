package com.kwok.mpgenerator.web.controller;

import com.kwok.mpgenerator.web.entity.User;
import com.kwok.mpgenerator.web.entity.dto.UserDto;
import com.kwok.mpgenerator.web.entity.query.UserQuery;
import com.kwok.mpgenerator.web.entity.form.UserForm;
import com.kwok.mpgenerator.web.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.kwok.mpgenerator.lang.ResponseDto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
* 用户表 控制类
* @author kwok
* @date 2023-12-28
*/
@Api(value = "UserController",tags = "用户表")
@RestController
@RequestMapping("userService")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("新增_用户表信息")
    @PostMapping("/addUser")
    public ResponseDto<Long> addUser(@RequestBody UserForm form) {

        Long id = userService.addUser(form);
        return  ResponseDto.success(id);
    }

    @ApiOperation("查询_用户表信息")
    @PostMapping("/getPageList")
    public ResponseDto<Page<UserDto>> getPageList(@RequestBody UserQuery query){
        Page<UserDto> dto = userService.getPageList(query);
        return  ResponseDto.success(dto);
    }

    @ApiOperation("修改_用户表信息")
    @PostMapping("/updateUser")
    public ResponseDto<Long> updateUser(@RequestBody UserForm form) {

        Long id = userService.updateUser(form);
        return  ResponseDto.success(id);
    }

    @ApiOperation("根据id查询_用户表信息")
    @GetMapping("/getUserById/{id}")
    public ResponseDto<UserDto> getUserById(@PathVariable Long id) {

        UserDto dto = userService.getUserById(id);
        return  ResponseDto.success(dto);
    }

    @ApiOperation("删除_用户表信息")
    @PostMapping("/removeUser/{id}")
    public ResponseDto<Long> removeUser(@PathVariable Long id) {

        userService.removeUser(id);
        return  ResponseDto.success(id);
    }

}
