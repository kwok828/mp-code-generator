package com.kwok.mpgenerator.web.mapper;


import com.kwok.mpgenerator.web.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
  * 用户表 数据映射接口
  * @author kwok
  * @date 2023-12-28
*/
@Mapper
public interface UserMapper extends BaseMapper<User>{

}


