package com.kwok.mpgenerator.web.service;

import com.kwok.mpgenerator.web.entity.User;
import com.kwok.mpgenerator.web.entity.dto.UserDto;
import com.kwok.mpgenerator.web.entity.query.UserQuery;
import com.kwok.mpgenerator.web.entity.form.UserForm;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.RequestBody;

/**
* 用户表 服务接口
* @author kwok
* @date 2023-12-28
*/
public interface UserService extends IService<User>{

    /**
      * 新增
      * @param form 表单参数
      * @return 新增结果
    */
    Long addUser(UserForm form);

    /**
      * 分页条件查询
      * @param query 查询参数
      * @return 分页结果
    */
    Page<UserDto> getPageList(@RequestBody UserQuery query);

    /**
      * 修改
      * @param form 表单参数
      * @return 修改结果
    */
    Long updateUser(UserForm form);

    /**
    * 根据id查询详情
    * @param id 序号
    * @return 查询结果
    */
    UserDto getUserById(Long id);

    /**
    * 根据id删除
    * @param id 序号
    * @return 删除结果
    */
    Integer removeUser(Long id);

}
