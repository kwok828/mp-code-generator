package com.kwok.mpgenerator.web.entity.form;

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;
import java.util.stream.Collectors;

/**
*   UserForm  表单参数
*   @author kwok
*   @date 2023-12-28
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class UserForm {

    /**
      *  序号
    */
    private Long id;
    /**
      *  名称
    */
    private String name;
    /**
      *  年龄
    */
    private Integer age;
    /**
      *  性别
    */
    private Integer sex;
    /**
      *  备注
    */
    private String remark;
}