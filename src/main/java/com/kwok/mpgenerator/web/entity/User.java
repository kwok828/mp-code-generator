package com.kwok.mpgenerator.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
*   用户表  实体类
*   @author kwok
*   @date 2023-12-28
*/
@Data
@TableName("sys_user")
public class User extends AuditObject implements Serializable {

private static final long serialVersionUID = 1L;

    /**
      *  名称
    */
    @TableField("name")
    private String name;
    /**
      *  年龄
    */
    @TableField("age")
    private Integer age;
    /**
      *  性别
    */
    @TableField("sex")
    private Integer sex;

}
