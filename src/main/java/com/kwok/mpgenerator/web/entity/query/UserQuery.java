package com.kwok.mpgenerator.web.entity.query;

import com.kwok.mpgenerator.web.entity.User;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import java.util.Objects;

/**
*   UserQuery  查询参数
*   @author kwok
*   @date 2023-12-28
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class UserQuery {

    /**
      *  序号
    */
    private Long id;
    /**
      *  名称
    */
    private String name;
    /**
      *  年龄
    */
    private Integer age;
    /**
      *  性别
    */
    private Integer sex;

    /** 当前分页 */
    private Integer pageNum;
    /** 每页大小 */
    private Integer pageSize;

    public static LambdaQueryWrapper<User> getQueryWrapper(UserQuery query){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Objects.nonNull(query.getId()), User::getId, query.getId());
        queryWrapper.like(StringUtils.isNotEmpty(query.getName()), User::getName, query.getName());
        queryWrapper.eq(Objects.nonNull(query.getAge()), User::getAge, query.getAge());
        queryWrapper.eq(User::getDelFlag, 0);
        queryWrapper.orderByDesc(User::getId);
        return queryWrapper;
    }

}