package com.kwok.mpgenerator.web.entity.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;
import com.google.common.collect.Lists;
import java.time.LocalDateTime;
import com.kwok.mpgenerator.web.entity.User;

import java.util.List;
import java.util.stream.Collectors;

/**
*   UserDto  dto类
*   @author kwok
*   @date 2023-12-28
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDto {

    /**
      *  序号
    */
    private Long id;
    /**
      *  名称
    */
    private String name;
    /**
      *  年龄
    */
    private Integer age;
    /**
      *  性别
    */
    private Integer sex;
    /**
      *  创建时间
    */
    private LocalDateTime createTime;
    /**
      *  备注
    */
    private String remark;

    public static UserDto toDto(User entity){
        if (ObjectUtils.isEmpty(entity)) {
            return null;
        }
        UserDto dto = new UserDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public static List<UserDto> toDtoList(List<User> entityList){
        if (CollectionUtils.isEmpty(entityList)) {
            return Lists.newArrayList();
        }
        return entityList.stream().map(UserDto::toDto).collect(Collectors.toList());
    }

}