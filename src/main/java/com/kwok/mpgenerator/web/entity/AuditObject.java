package com.kwok.mpgenerator.web.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

import java.io.Serializable;

/**
*   审计类
*   @author kwok
*   @date 2023-12-28
*/
@Data
public class AuditObject implements Serializable {

private static final long serialVersionUID = 1L;

    /**
      *  序号
    */
    @TableId(value = "id", type = IdType.AUTO)
    @TableField("id")
    private Long id;
    /**
      *  创建时间
    */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /**
      *  创建人
    */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;
    /**
      *  更新时间
    */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    /**
      *  更新人
    */
    @TableField(value = "update_by", fill = FieldFill.INSERT_UPDATE)
    private String updateBy;
    /**
      *  备注
    */
    private String remark;
    /**
      *  逻辑删除（0 未删除 1 已删除）
    */
    @TableLogic
    @TableField("del_flag")
    private Boolean delFlag;
    /**
      *  版本号
    */
    @Version
    @TableField("version")
    private Integer version;

}
