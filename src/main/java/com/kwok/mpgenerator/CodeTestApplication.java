package com.kwok.mpgenerator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动类
 */
@EnableTransactionManagement
@SpringBootApplication
@MapperScan("com.kwok.mpgenerator.*.mapper")
@ComponentScan(basePackages = {"com.kwok.**"})
public class CodeTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CodeTestApplication.class, args);
    }
}
