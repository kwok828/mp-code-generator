package com.kwok.mybatisgenerator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;

/**
 * mp代码生成
 */
public class MybatisAutoGenerator {

    public static void main(String[] args) {

        //构建代码生成器对象
        AutoGenerator mpg = new AutoGenerator();
        //1、全局配置
        GlobalConfig gc = new GlobalConfig();
        String oPath = System.getProperty("user.dir");//得到当前项目的路径
        String srcPath = oPath + "/src/main/java";//src 路径

        gc.setOutputDir(oPath + "/src/main/java");//生成文件的输出目录

        // 自定义路径（需修改）
        String resourcesPath = oPath + "/src/main/resources/"; //resources 路径
        String codePackageName = "web"; //代码所在包名
        String codePackageParentPath = "com.kwok.mybatisgenerator"; // 代码所在包父包名
        String codePackagePath = srcPath + "/com/kwok/mybatisgenerator/" + codePackageName; //代码所在路径

        gc.setAuthor("kwok");//作者
        gc.setOpen(false);//是否打开输出目录
        gc.setFileOverride(false);//是否覆盖已有的文件
        gc.setServiceName("%sService");//去除Service的I前缀
        gc.setIdType(IdType.AUTO);//主键生成策略
        //ONLY_DATE 只使用 java.util.date 代替，SQL_PACK 使用 java.sql 包下的，TIME_PACK 使用 java.time 包下的 java8 新的时间类型
        gc.setDateType(DateType.TIME_PACK);//数据库时间类型 到 实体类时间类型 对应策略
        gc.setSwagger2(true);//开启swagger2模式
        mpg.setGlobalConfig(gc);

        //2、数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        //dsc.setUrl("jdbc:mysql://localhost:3306/mybatis_plus?useSSl=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC");
        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/mp-generator?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=Asia/Shanghai");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        dsc.setDbType(DbType.MYSQL);//数据库类型
        mpg.setDataSource(dsc);

        //3、包的配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName(codePackageName);//父包模块名
        pc.setParent(codePackageParentPath);//父包名,如果为空，将下面子包名必须写全部， 否则就只需写子包名
        pc.setEntity("entity");
        pc.setMapper("mapper");
        pc.setService("service");
        pc.setController("controller");

        mpg.setPackageInfo(pc);

        //4、策略配置
        StrategyConfig sy = new StrategyConfig();
        sy.setInclude("sys_user");//设置要映射的表，可以设置多张
        sy.setNaming(NamingStrategy.underline_to_camel);//从数据库表到文件的命名策略,下划线转驼峰命名
        sy.setColumnNaming(NamingStrategy.underline_to_camel);//列的命名策略
        sy.setEntityLombokModel(true);//开启lombok支持
        sy.setLogicDeleteFieldName("del_flag");//设置逻辑删除字段
        sy.setVersionFieldName("version");//设置乐观锁
        sy.setRestControllerStyle(true);//开启controller的restful命名
        sy.setControllerMappingHyphenStyle(false);//开启controller中请求映射的连字符样式，如：localhost:8080/hello_id_1
        sy.setEntityBooleanColumnRemoveIsPrefix(false);
        sy.isEntityTableFieldAnnotationEnable();
        sy.setTablePrefix("sys_"); //过滤表前缀，生成实体类才不是TblUserInfo


        //设置自动填充字段
       TableFill create_time = new TableFill("create_time", FieldFill.INSERT);
       TableFill create_by = new TableFill("create_by", FieldFill.INSERT);
        TableFill update_time = new TableFill("update_time", FieldFill.INSERT_UPDATE);
        TableFill update_by = new TableFill("update_by", FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFills = new ArrayList<>();
        tableFills.add(create_time);
        tableFills.add(create_by);
        tableFills.add(update_time);
        tableFills.add(update_by);

        //创建模板
        TemplateConfig templateConfig = new TemplateConfig();
        //templateConfig.setController("templatespath/controller222.java");

        templateConfig.setController(null);
        templateConfig.setEntity(null);
        templateConfig.setService(null);
        templateConfig.setServiceImpl(null);
        templateConfig.setMapper(null);
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);
        mpg.setStrategy(sy);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();
        focList.add(new FileOutConfig("templatesPath/controller.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return codePackagePath + "/controller/" + tableInfo.getControllerName() + StringPool.DOT_JAVA;
            }
        });
        focList.add(new FileOutConfig("templatesPath/entity.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return codePackagePath + "/entity/" + tableInfo.getEntityName() + StringPool.DOT_JAVA;
            }
        });
        focList.add(new FileOutConfig("templatesPath/service.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return codePackagePath + "/service/" + tableInfo.getServiceName() + StringPool.DOT_JAVA;
            }
        });
        focList.add(new FileOutConfig("templatesPath/serviceImpl.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return codePackagePath + "/service/impl/" + tableInfo.getServiceImplName() + StringPool.DOT_JAVA;
            }
        });
        focList.add(new FileOutConfig("templatesPath/mapper.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return codePackagePath + "/mapper/" + tableInfo.getMapperName() + StringPool.DOT_JAVA;
            }
        });
        focList.add(new FileOutConfig("templatesPath/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return resourcesPath + "/mapper/" + tableInfo.getMapperName() + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);

        mpg.setCfg(cfg);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());

        //执行代码生成器
        mpg.execute();
    }

}
