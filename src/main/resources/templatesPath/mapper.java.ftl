package ${package.Mapper};


import ${package.Entity}.${entity};

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
  * ${table.comment} 数据映射接口
  * @author ${author}
  * @date ${date}
*/
@Repository
public interface ${table.mapperName}{

    Integer insert${table.entityName}(@Param("${table.entityName?uncap_first}") ${table.entityName} ${table.entityName?uncap_first});

    Integer delete${table.entityName}(@Param("id")Integer id);

    Integer update${table.entityName}(@Param("${table.entityName?uncap_first}")${table.entityName} ${table.entityName?uncap_first});

    List<${table.entityName}> get${table.entityName}List(<#list table.fields as field><#if field_index != 0>@Param("${field.propertyName}") ${field.propertyType} ${field.propertyName},</#if></#list>@Param("limit") Integer limit, @Param("offset") Integer offset);

    Integer get${table.entityName}Count(<#list table.fields as field><#if field_index == table.fields?size - 1>@Param("${field.propertyName}") ${field.propertyType} ${field.propertyName}<#elseif field_index != 0>@Param("${field.propertyName}") ${field.propertyType} ${field.propertyName}, </#if></#list>);
}


