package ${package.Service};

import ${package.Entity}.${entity};

/**
  * ${table.comment} 服务接口
  * @author ${author}
  * @date ${date}
*/
public interface ${table.serviceName}{

/**
  * 新增
  * @param ${table.entityName?uncap_first} ${table.comment}对象
  * @return 新增结果
*/
String add${table.entityName}(${table.entityName} ${table.entityName?uncap_first});

/**
  * 删除
  * @param id 序列ID
  * @return 删除结果
*/
String delete${table.entityName}(Integer id);

/**
  * 更新
  * @param ${table.entityName?uncap_first} ${table.comment}对象
  * @return 更新结果
*/
String update${table.entityName}(${table.entityName} ${table.entityName?uncap_first});

/**
* 查询列表
<#list table.fields as field>
    <#if field_index!=0>
        * @param ${field.propertyName} ${field.comment}
    </#if>
</#list>
* @param limit 查询多少行
* @param offset 起始查询位置
* @return 查询列表结果
*/
String get${table.entityName}List(<#list table.fields as field><#if field_index != 0>${field.propertyType} ${field.propertyName},</#if></#list>Integer limit, Integer offset);

/**
* 查询数量
<#list table.fields as field>
    <#if field_index!=0>
        * @param ${field.propertyName} ${field.comment}
    </#if>
</#list>
* @return 查询数量结果
*/
String get${table.entityName}Count(<#list table.fields as field><#if field_index == table.fields?size - 1>${field.propertyType} ${field.propertyName}<#elseif field_index != 0>${field.propertyType} ${field.propertyName}, </#if></#list>);


}
