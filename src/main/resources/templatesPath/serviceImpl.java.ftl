package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${package.Entity}.Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

/**
  * ${table.comment} 服务实现类
  * @author ${author}
  * @date ${date}
*/
@Service
public class ${table.serviceImplName} implements ${table.serviceName} {

    @Autowired
    private ${table.mapperName} ${table.mapperName?uncap_first};

    @Override
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000,rollbackFor=Exception.class)
    public String add${table.entityName}(${table.entityName} ${table.entityName?uncap_first}){
        try {
            Integer count = ${table.mapperName?uncap_first}.insert${table.entityName}(${table.entityName?uncap_first});
            if (count != null && count > 0){
                return new Result<>(true,"新增成功").toString();
            }else {
                return new Result<>(false,"新增失败").toString();
            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(false,"新增异常").toString();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000,rollbackFor=Exception.class)
    public String delete${table.entityName}(Integer id){
        try {
            Integer count = ${table.mapperName?uncap_first}.delete${table.entityName}(id);
            if (count != null && count > 0){
                return new Result<>(true,"删除成功").toString();
            }else {
                return new Result<>(false,"删除失败").toString();
            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(false,"删除异常").toString();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000,rollbackFor=Exception.class)
    public String update${table.entityName}(${table.entityName} ${table.entityName?uncap_first}){
        try {
            Integer count = ${table.mapperName?uncap_first}.update${table.entityName}(${table.entityName?uncap_first});
            if (count != null && count > 0){
                return new Result<>(true,"修改成功").toString();
            }else {
                return new Result<>(false,"修改失败").toString();
            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(false,"修改异常").toString();
        }
    }

    @Override
    public String get${table.entityName}List(<#list table.fields as field><#if field_index != 0>${field.propertyType} ${field.propertyName},</#if></#list>Integer limit, Integer offset){
        try {
            List<${table.entityName}> ${table.entityName?uncap_first}List = ${table.mapperName?uncap_first}.get${table.entityName}List(<#list table.fields as field><#if field_index != 0>${field.propertyName},</#if></#list> limit, offset);
            if (${table.entityName?uncap_first}List != null && ${table.entityName?uncap_first}List.size() > 0){
                return new Result<>(true,"查询数据成功",${table.entityName?uncap_first}List).toString();
            }else {
                return new Result<>(true,"查询数据为空").toString();
            }
        }catch (Exception e){
            e.printStackTrace();
            return new Result<>(false,"查询异常").toString();
        }
    }

    @Override
    public String get${table.entityName}Count(<#list table.fields as field><#if field_index == table.fields?size - 1>${field.propertyType} ${field.propertyName}<#elseif field_index != 0>${field.propertyType} ${field.propertyName}, </#if></#list>){
        try {
            Integer count = ${table.mapperName?uncap_first}.get${table.entityName}Count(<#list table.fields as field><#if field_index == table.fields?size - 1>${field.propertyName}<#elseif field_index != 0>${field.propertyName}, </#if></#list>);
            if (count != null && count > 0){
                return new Result<>(true,"查询成功",count).toString();
            }else {
                return new Result<>(true,"查询数据为空",0).toString();
            }
        }catch (Exception e){
            e.printStackTrace();
            return new Result<>(false,"查询异常").toString();
        }
    }
}
