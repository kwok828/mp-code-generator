package ${package.Controller};

import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
<#if restControllerStyle>
<#else>
    import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
    import ${superControllerClassPackage};
</#if>
/**
  * ${table.comment} 控制类
  * @author ${author}
  * @date ${date}
*/
@Api(value = "${table.controllerName}",tags = "${table.comment}")
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("${table.serviceName?uncap_first}")
public class ${table.controllerName} {

    @Autowired
    private ${table.serviceName} ${table.serviceName?uncap_first};

    @ApiOperation("新增_${table.comment}")
    @PostMapping("/add<#if controllerMappingHyphenStyle??>${controllerMappingHyphen?cap_first}<#else>${table.entityPath?cap_first}</#if>")
    public String add${table.entityName?cap_first}(@RequestBody ${table.entityName} ${table.entityName?uncap_first}) {

        String massage = ${table.entityName?uncap_first}Service.add${table.entityName?cap_first}(${table.entityName?uncap_first});
        return  massage;
    }

    @ApiOperation("删除_${table.comment}（通过序列ID）")
    @DeleteMapping(value = "/delete<#if controllerMappingHyphenStyle??>${controllerMappingHyphen?cap_first}<#else>${table.entityPath?cap_first}</#if>/{id}")
    public String delete${table.entityName?cap_first}(@NotNull @PathVariable Integer id) {

        String massage = ${table.entityName?uncap_first}Service.delete${table.entityName?cap_first}(id);
        return massage;
    }

    @ApiOperation("修改_${table.comment}")
    @PutMapping("update${table.entityName?cap_first}")
    public String update${table.entityName?cap_first}(@Valid @RequestBody ${table.entityName} ${table.entityName?uncap_first}) {

        String massage = ${table.entityName?uncap_first}Service.update${table.entityName?cap_first}(${table.entityName?uncap_first});
        return massage;
    }

    @ApiOperation("查询列表_${table.comment}：{列表}")
    @ApiImplicitParams({
        <#list table.fields as field>
            <#if field_index!=0>
                @ApiImplicitParam(name = "${field.propertyName}", value = "${field.comment}", dataType = "${field.propertyType}"),
            </#if>
        </#list>
                @ApiImplicitParam(name = "limit", value = "查询多少行", dataType = "Integer"),
                @ApiImplicitParam(name = "offset", value = "起始查询位置", dataType = "Integer")
    })
    @GetMapping("get${table.entityName?cap_first}List")
    public String get${table.entityName?cap_first}List(
        <#list table.fields as field>
            <#if field_index != 0>
                @RequestParam(name="${field.propertyName}", required = false) ${field.propertyType} ${field.propertyName},
            </#if>
        </#list>
                @RequestParam(name="limit",required = false)Integer limit,
                @RequestParam(name="offset",required = false)Integer offset){

        String massage = ${table.entityName?uncap_first}Service.get${table.entityName?cap_first}List(<#list table.fields as field><#if field_index != 0>${field.propertyName},</#if></#list>limit,offset);
        return massage;
    }

    @ApiOperation("查询数量_${table.comment}：{数量}")
    @ApiImplicitParams({
        <#list table.fields as field>
            <#if field_index!=0>
                @ApiImplicitParam(name = "${field.propertyName}", value = "${field.comment}", dataType = "${field.propertyType}"),
            </#if>
        </#list>
    })
    @GetMapping("get${table.entityName?cap_first}Count")
    public String get${table.entityName?cap_first}Count(
        <#list table.fields as field>
            <#if field_index == table.fields?size - 1>
                @RequestParam(name = "${field.propertyName}", required = false) ${field.propertyType} ${field.propertyName}) {
            <#elseif field_index != 0>
                @RequestParam(name = "${field.propertyName}", required = false) ${field.propertyType} ${field.propertyName},
            </#if>
        </#list>

        String massage = ${table.entityName?uncap_first}Service.get${table.entityName?cap_first}Count(<#list table.fields as field><#if field_index == table.fields?size - 1>${field.propertyName}<#elseif field_index != 0>${field.propertyName}, </#if></#list>);
        return massage;
    }

}
