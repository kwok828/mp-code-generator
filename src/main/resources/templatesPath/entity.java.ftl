package ${package.Entity};

import lombok.Data;
import java.util.Date;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
*   ${table.comment}  实体类
*   @author ${author}
*   @date ${date}
*/
@Data
public class ${entity} implements Serializable {

private static final long serialVersionUID = 1L;

<#list table.fields as value>
    <#if value_index == 0>
        <#if value.comment!="">
            /**
            *  ${value.comment}
            */
        </#if>
        @NotNull
        private ${value.propertyType} ${value.capitalName};
    <#else>
        <#if value.comment!="">
            /**
            *  ${value.comment}
            */
        </#if>
        private ${value.propertyType} ${value.capitalName};
    </#if>
</#list>

}
