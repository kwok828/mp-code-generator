<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package.Mapper}.${table.mapperName}">

    <insert id="insert${table.entityName}" useGeneratedKeys="true" keyProperty="${table.entityName?uncap_first}.id">
        insert into ${table.name}(<#list table.fields as field><#if field_index == table.fields?size - 1>${field.name}<#elseif field_index != 0>${field.name}, </#if></#list>)
        values(<#list table.fields as field><#if field_index == table.fields?size - 1><#noparse>#</#noparse>{${table.entityName?uncap_first}.${field.propertyName}}<#elseif field_index != 0><#noparse>#</#noparse>{${table.entityName?uncap_first}.${field.propertyName}}, </#if></#list>)
    </insert>
    <delete id="delete${table.entityName}">
        delete from ${table.name}
        where id = <#noparse>#</#noparse>{id}
    </delete>
    <update id="update${table.entityName}">
        update ${table.name}
        <set>
            <#list table.fields as field>
                <#if field_index != 0>
                    <if test="${table.entityName?uncap_first}.${field.propertyName} != null">
                        ,${field.name} = <#noparse>#</#noparse>{${table.entityName?uncap_first}.${field.propertyName}}
                    </if>
                </#if>
            </#list>
        </set>
        where id = <#noparse>#</#noparse>{${table.entityName?uncap_first}.id}
    </update>

    <resultMap id = "Basic${table.entityName}Map" type = "${package.Entity}.${table.entityName}">
        <#list table.fields as field>
            <#if field_index gte 0>
                <result column="${field.name}" property="${field.propertyName}"/>
            </#if>
        </#list>
    </resultMap>
    <select id="get${table.entityName}List" resultMap="Basic${table.entityName}Map">
        select <#list table.fields as field><#if field_index == table.fields?size - 1>${field.name}<#elseif field_index gte 0>${field.name}, </#if></#list>
        from ${table.name}
        <where>
            <#list table.fields as field>
                <#if field_index != 0>
                    <if test="${field.propertyName} != null">
                        ,${field.name} = <#noparse>#</#noparse>{${field.propertyName}}
                    </if>
                </#if>
            </#list>
        </where>
    </select>
    <select id="get${table.entityName}Count" resultType="java.lang.Integer">
        select count(id)
        from ${table.name}
        <where>
            <#list table.fields as field>
                <#if field_index != 0>
                    <if test="${field.propertyName} != null">
                        ,${field.name} = <#noparse>#</#noparse>{${field.propertyName}}
                    </if>
                </#if>
            </#list>
        </where>
    </select>

</mapper>
