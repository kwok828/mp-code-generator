package ${package.Controller};

import ${package.Entity}.${entity};
import ${package.Entity}.dto.${entity}Dto;
import ${package.Entity}.query.${entity}Query;
import ${package.Entity}.form.${entity}Form;
import ${package.Service}.${table.serviceName};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.kwok.mpgenerator.lang.ResponseDto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

<#if restControllerStyle>
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
/**
* ${table.comment} 控制类
* @author ${author}
* @date ${date}
*/
@Api(value = "${table.controllerName}",tags = "${table.comment}")
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("${table.serviceName?uncap_first}")
public class ${table.controllerName} {

    @Autowired
    private ${table.serviceName} ${table.serviceName?uncap_first};

    @ApiOperation("新增_${table.comment}信息")
    @PostMapping("/add${table.entityName}")
    public ResponseDto<Long> add${table.entityName}(@RequestBody ${table.entityName}Form form) {

        Long id = ${table.entityName?uncap_first}Service.add${table.entityName}(form);
        return  ResponseDto.success(id);
    }

    @ApiOperation("查询_${table.comment}信息")
    @PostMapping("/getPageList")
    public ResponseDto<Page<${table.entityName}Dto>> getPageList(@RequestBody ${table.entityName}Query query){
        Page<${table.entityName}Dto> dto = ${table.entityName?uncap_first}Service.getPageList(query);
        return  ResponseDto.success(dto);
    }

    @ApiOperation("修改_${table.comment}信息")
    @PostMapping("/update${table.entityName}")
    public ResponseDto<Long> update${table.entityName}(@RequestBody ${table.entityName}Form form) {

        Long id = ${table.entityName?uncap_first}Service.update${table.entityName}(form);
        return  ResponseDto.success(id);
    }

    @ApiOperation("根据id查询_${table.comment}信息")
    @GetMapping("/get${table.entityName}ById/{id}")
    public ResponseDto<${table.entityName}Dto> get${table.entityName}ById(@PathVariable Long id) {

        ${table.entityName}Dto dto = ${table.entityName?uncap_first}Service.get${table.entityName}ById(id);
        return  ResponseDto.success(dto);
    }

    @ApiOperation("删除_${table.comment}信息")
    @PostMapping("/remove${table.entityName}/{id}")
    public ResponseDto<Long> remove${table.entityName}(@PathVariable Long id) {

        ${table.entityName?uncap_first}Service.remove${table.entityName}(id);
        return  ResponseDto.success(id);
    }

}
