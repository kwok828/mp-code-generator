package ${package.Entity}.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;
import com.google.common.collect.Lists;
import java.time.LocalDateTime;
import ${package.Entity}.${entity};

import java.util.List;
import java.util.stream.Collectors;

/**
*   ${entity}Dto  dto类
*   @author ${author}
*   @date ${date}
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class ${entity}Dto {

<#list table.fields as value>
    <#if value.comment !=""
    && value.propertyName !="createBy" && value.propertyName !="updateTime" && value.propertyName !="updateBy"
    && value.propertyName !="delFlag" && value.propertyName !="version">
    /**
      *  ${value.comment}
    */
    </#if>
    <#if value.propertyName !="createBy" && value.propertyName !="updateTime" && value.propertyName !="updateBy"
    && value.propertyName !="delFlag" && value.propertyName !="version">
    private ${value.propertyType} ${value.propertyName};
    </#if>
</#list>

    public static ${entity}Dto toDto(${entity} entity){
        if (ObjectUtils.isEmpty(entity)) {
            return null;
        }
        ${entity}Dto dto = new ${entity}Dto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public static List<${entity}Dto> toDtoList(List<${entity}> entityList){
        if (CollectionUtils.isEmpty(entityList)) {
            return Lists.newArrayList();
        }
        return entityList.stream().map(${entity}Dto::toDto).collect(Collectors.toList());
    }

}