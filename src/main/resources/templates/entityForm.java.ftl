package ${package.Entity}.form;

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;
import java.util.stream.Collectors;

/**
*   ${entity}Form  表单参数
*   @author ${author}
*   @date ${date}
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class ${entity}Form {

<#list table.fields as value>
    <#if value.comment !=""
    && value.propertyName !="createTime" && value.propertyName !="createBy" && value.propertyName !="updateTime" && value.propertyName !="updateBy"
    && value.propertyName !="delFlag" && value.propertyName !="version">
    /**
      *  ${value.comment}
    */
    </#if>
    <#if value.propertyName !="createTime" && value.propertyName !="createBy" && value.propertyName !="updateTime" && value.propertyName !="updateBy"
    && value.propertyName !="delFlag" && value.propertyName !="version">
    private ${value.propertyType} ${value.propertyName};
    </#if>
</#list>
}