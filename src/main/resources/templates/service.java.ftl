package ${package.Service};

import ${package.Entity}.${entity};
import ${package.Entity}.dto.${entity}Dto;
import ${package.Entity}.query.${entity}Query;
import ${package.Entity}.form.${entity}Form;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.RequestBody;

/**
* ${table.comment} 服务接口
* @author ${author}
* @date ${date}
*/
public interface ${table.serviceName} extends IService<${table.entityName}>{

    /**
      * 新增
      * @param form 表单参数
      * @return 新增结果
    */
    Long add${table.entityName}(${table.entityName}Form form);

    /**
      * 分页条件查询
      * @param query 查询参数
      * @return 分页结果
    */
    Page<${table.entityName}Dto> getPageList(@RequestBody ${table.entityName}Query query);

    /**
      * 修改
      * @param form 表单参数
      * @return 修改结果
    */
    Long update${table.entityName}(${table.entityName}Form form);

    /**
    * 根据id查询详情
    * @param id 序号
    * @return 查询结果
    */
    ${table.entityName}Dto get${table.entityName}ById(Long id);

    /**
    * 根据id删除
    * @param id 序号
    * @return 删除结果
    */
    Integer remove${table.entityName}(Long id);

}
