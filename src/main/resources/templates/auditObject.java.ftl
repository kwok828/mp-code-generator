package ${package.Entity};

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

import java.io.Serializable;

/**
*   审计类
*   @author ${author}
*   @date ${date}
*/
@Data
public class AuditObject implements Serializable {

private static final long serialVersionUID = 1L;

<#list table.fields as value>
    <#if value.comment!=""
    && (value.propertyName =="id" || value.propertyName =="createTime" || value.propertyName =="createBy" || value.propertyName =="updateTime"
    || value.propertyName =="updateBy" || value.propertyName =="remark" || value.propertyName =="delFlag" || value.propertyName =="version")>
    /**
      *  ${value.comment}
    */
    </#if>
    <#if value.propertyName =="id">
    @TableId(value = "id", type = IdType.AUTO)
    @TableField("${value.name}")
    </#if>
    <#if value.propertyName =="createTime" || value.propertyName =="createBy">
    @TableField(value = "${value.name}", fill = FieldFill.INSERT)
    </#if>
    <#if value.propertyName =="updateTime" || value.propertyName =="updateBy">
    @TableField(value = "${value.name}", fill = FieldFill.INSERT_UPDATE)
    </#if>
    <#if value.propertyName =="delFlag">
    @TableLogic
    @TableField("${value.name}")
    </#if>
    <#if value.propertyName =="version">
    @Version
    @TableField("${value.name}")
    </#if>
    <#if value.propertyName =="id" || value.propertyName =="createTime" || value.propertyName =="createBy" || value.propertyName =="updateTime"
         || value.propertyName =="updateBy" || value.propertyName =="remark" || value.propertyName =="delFlag" || value.propertyName =="version">
    private ${value.propertyType} ${value.propertyName};
    </#if>
</#list>

}
