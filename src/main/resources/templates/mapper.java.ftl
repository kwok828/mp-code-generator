package ${package.Mapper};


import ${package.Entity}.${entity};
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
  * ${table.comment} 数据映射接口
  * @author ${author}
  * @date ${date}
*/
@Mapper
public interface ${table.mapperName} extends BaseMapper<${table.entityName}>{

}


