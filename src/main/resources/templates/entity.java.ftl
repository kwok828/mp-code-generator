package ${package.Entity};

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
*   ${table.comment}  实体类
*   @author ${author}
*   @date ${date}
*/
@Data
@TableName("${table.name}")
public class ${entity} extends AuditObject implements Serializable {

private static final long serialVersionUID = 1L;

<#list table.fields as value>
    <#if value.comment !=""
    && value.propertyName !="id" && value.propertyName !="createTime" && value.propertyName !="createBy" && value.propertyName !="updateTime"
    && value.propertyName !="updateBy" && value.propertyName !="remark" && value.propertyName !="delFlag" && value.propertyName !="version">
    /**
      *  ${value.comment}
    */
    </#if>
    <#if value.propertyName!="id" && value.propertyName!="createTime" && value.propertyName!="createBy" && value.propertyName!="updateTime"
         && value.propertyName!="updateBy" && value.propertyName!="remark" && value.propertyName!="delFlag" && value.propertyName!="version">
    @TableField("${value.name}")
    private ${value.propertyType} ${value.propertyName};
    </#if>
</#list>

}
