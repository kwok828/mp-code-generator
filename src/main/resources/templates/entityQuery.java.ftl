package ${package.Entity}.query;

import ${package.Entity}.${entity};
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import java.util.Objects;

/**
*   ${entity}Query  查询参数
*   @author ${author}
*   @date ${date}
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class ${entity}Query {

<#list table.fields as value>
    <#if value.comment !=""
    && value.propertyName !="createTime" && value.propertyName !="createBy" && value.propertyName !="updateTime" && value.propertyName !="updateBy"
    && value.propertyName !="remark" && value.propertyName !="delFlag" && value.propertyName !="version">
    /**
      *  ${value.comment}
    */
    </#if>
    <#if value.propertyName !="createTime" && value.propertyName !="createBy" && value.propertyName !="updateTime" && value.propertyName !="updateBy"
    && value.propertyName !="remark" && value.propertyName !="delFlag" && value.propertyName !="version">
    private ${value.propertyType} ${value.propertyName};
    </#if>
</#list>

    /** 当前分页 */
    private Integer pageNum;
    /** 每页大小 */
    private Integer pageSize;

    public static LambdaQueryWrapper<${entity}> getQueryWrapper(${entity}Query query){
        LambdaQueryWrapper<${entity}> queryWrapper = new LambdaQueryWrapper<>();
        <#list table.fields as value>
            <#if value.propertyName !="createTime" && value.propertyName !="createBy" && value.propertyName !="updateTime"
            && value.propertyName !="updateBy" && value.propertyName !="remark" && value.propertyName !="delFlag" && value.propertyName !="version">
                <#if value.propertyType == "Long" || value.propertyType == "Integer">
        queryWrapper.eq(Objects.nonNull(query.get${value.capitalName}()), ${entity}::get${value.capitalName}, query.get${value.capitalName}());
                </#if>
                <#if value.propertyType == "String">
        queryWrapper.like(StringUtils.isNotEmpty(query.get${value.capitalName}()), ${entity}::get${value.capitalName}, query.get${value.capitalName}());
                </#if>
            </#if>
        </#list>
        queryWrapper.eq(${entity}::getDelFlag, 0);
        queryWrapper.orderByDesc(${entity}::getId);
        return queryWrapper;
    }

}