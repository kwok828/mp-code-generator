package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Entity}.dto.${entity}Dto;
import ${package.Entity}.query.${entity}Query;
import ${package.Entity}.form.${entity}Form;
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
* ${table.comment} 服务实现类
* @author ${author}
* @date ${date}
*/
@Service
public class ${table.serviceImplName} extends ServiceImpl<${table.mapperName}, ${table.entityName}> implements ${table.serviceName} {

    @Autowired
    private ${table.mapperName} ${table.mapperName?uncap_first};

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Long add${table.entityName}(${table.entityName}Form form){
        ${table.entityName} ${table.entityName?uncap_first} = new ${table.entityName}();
        BeanUtils.copyProperties(form, ${table.entityName?uncap_first});
        int insert = ${table.mapperName?uncap_first}.insert(${table.entityName?uncap_first});
        return ${table.entityName?uncap_first}.getId();
    }

    @Override
    public Page<${table.entityName}Dto> getPageList(@RequestBody ${table.entityName}Query query){
        Page<${table.entityName}> page = new Page<>(query.getPageNum(), query.getPageSize());
        LambdaQueryWrapper<${table.entityName}> queryWrapper = ${table.entityName}Query.getQueryWrapper(query);
        IPage<${table.entityName}> ${table.entityName?uncap_first}Page = ${table.mapperName?uncap_first}.selectPage(page, queryWrapper);
        Page<${table.entityName}Dto> ${table.entityName?uncap_first}DtoPage = new Page<>();
        BeanUtils.copyProperties(${table.entityName?uncap_first}Page, ${table.entityName?uncap_first}DtoPage);
        ${table.entityName?uncap_first}DtoPage.setRecords(${table.entityName}Dto.toDtoList(${table.entityName?uncap_first}Page.getRecords()));
        return ${table.entityName?uncap_first}DtoPage;
    }

    @Override
        @Transactional(rollbackFor=Exception.class)
        public Long update${table.entityName}(${table.entityName}Form form){
        ${table.entityName} ${table.entityName?uncap_first} = new ${table.entityName}();
        BeanUtils.copyProperties(form, ${table.entityName?uncap_first});
        int update = ${table.mapperName?uncap_first}.updateById(${table.entityName?uncap_first});
        return ${table.entityName?uncap_first}.getId();
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public ${table.entityName}Dto get${table.entityName}ById(Long id){
        ${table.entityName} entity = ${table.mapperName?uncap_first}.selectById(id);
        return ${table.entityName}Dto.toDto(entity);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Integer remove${table.entityName}(Long id){

        return ${table.mapperName?uncap_first}.deleteById(id);
    }

}
